dd if=/dev/zero of=toto bs=1024 count=10k               # create a 10MB file
losetup /dev/loop0 toto                                 # put the file in the loopback
mkfs -t ext2 /dev/loop0                                 # format the filesystem
mkdir /mnt/toto; mount /dev/loop0 /mnt/toto             # mount the FS
echo "hi Java class" > /mnt/toto/toto.txt               # copy the image in the FS
stat /mnt/toto/toto.txt                                 # find the offset
echo "hello world" > titi                               # create text file to hide
# end configuration FileSystem
# copy titi.txt after toto.txt
dd if=titi.txt of=/dev/loop0 bs=1 seek=2062             # put the image at the offset 1664 of the file
# retreive titi.txt from its size
dd if=/dev/loop0 of=titi2.txt bs=1 skip=2062 count=12   # retreive the image
# check diff
diff -s titi2.txt titi.txt                              # check if the file has not change
