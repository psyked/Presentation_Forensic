from scapy.all import *
import sys

if __name__ == "__main__":
    if len(sys.argv) < 3:
        print("usage : $scriptMrEnigma in.pcap out.png")
        exit()
    pcap = rdpcap(sys.argv[1])
    raws = [p[Raw] for p in pcap]
    raws = raws[::2]
    raws = [Raw(load=p.load[16:32]) for p in raws]
    open(sys.argv[2], 'w').write("".join(str(p) for p in raws))