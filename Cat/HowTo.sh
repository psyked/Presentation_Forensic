du -b 1.jpg                             # check info (size) of the image
du -b 0.jpg                             # check info (size) of the image
cat 0.jpg >> 1.jpg                      # concatenate image 0 after 1
du -b 1.jpg                             # the file is bigger
foremost 1.jpg                          # search for concatenated files
cp output/jpg/00000009.jpg foremost.jpg # copy content
tail -c 6015 1.jpg > tail.jpg           # fetch the last part of the file 6015 = size of 0.jpg
diff -s foremost.jpg 0.jpg              # compare
diff -s tail.jpg 0.jpg                  # both files
# clear
rm tail.jpg; rm foremost.jpg            #
rm -rf output; cp Source/1.jpg 1.jpg    #